package dev.gilang.wisatagk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.io.File;

//base activity digunakan untuk membuat variabel global yang dapat di akses seluruh turunan kelasnya
public class BaseActivity extends AppCompatActivity {
    //variabel deps yang akan di gunakan di setiap kelas untuk melakukan inject singleton service.java
    Deps deps;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        File cacheFile = new File(getCacheDir(), "responses");
        //varaibel deps di isi dengan variabel dari networkmodule
        deps  = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
    }

    public Deps getDeps() {
        return deps;
    }
}
