package dev.gilang.wisatagk;
/*
interface yang digunakan untuk mengirim data dari presenter ke activity bisa di sebut dengan view
 */
public interface WisataView {
    void onShowProgres();
    void onHideProgres();
    void onSuccess(AllWisataResponse allWisataResponse);
    void onFailed(String string);
}
