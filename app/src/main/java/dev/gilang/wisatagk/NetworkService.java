package dev.gilang.wisatagk;


import com.google.gson.JsonObject;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;
/*
reques ke server . pada proses ini retrofil otomatis mengubah
JSON menjadi class
 */
/* Disini proses parsing restrofit*/
public interface NetworkService{
    @POST("getAllWisata")
    Observable<AllWisataResponse> getAllWisata(@Body JsonObject jsonObject);
    @POST("search")
    Observable<AllWisataResponse> search(@Body JsonObject jsonObject);
    @POST("getByJenis")
    Observable<AllWisataResponse> getByJenis(@Body JsonObject jsonObject);
}
