package dev.gilang.wisatagk;

import javax.inject.Singleton;

import dagger.Component;

//singletone yang digunakan pada deps adalah networkmodule
@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    //beberap kelas yang di inject adalah 3 br
    void inject(MapsActivity activity);
    void inject(MainActivity activity);
    void inject(SearchActivity activity);
}
