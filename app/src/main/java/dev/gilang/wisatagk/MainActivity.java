package dev.gilang.wisatagk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/*
menampilkan data wisata berdasarkan jenis wisata yang di pilih
 */
public class MainActivity extends BaseActivity implements WisataView, LocationListener {
    @Inject public Service service;

    WisataPresenter presenter;
    RecyclerView rvListWisata;
    WisataAdapter adapter;
    List<AllWisataResponse.DataWisata> list = new ArrayList<>();
    ProgressDialog progressDialog;
    String jenis;
    double lat = 0.0D, lng = 0.0D;
    LocationManager locationManager;
    Location location;
    boolean permission_granted;
    boolean allwisata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = Helper.initProgressDialog(this, getString(R.string.please_wait));

        getSupportActionBar().setTitle(getString(R.string.list_wisata));

        if(checkPermission())loadView();

    }

    @SuppressLint("MissingPermission")
    private void loadView(){
        //untuk ambil lokasi pengguna
        assert locationManager != null;
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, DeskripsiActivity.REQUEST_TIME, DeskripsiActivity.DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, DeskripsiActivity.REQUEST_TIME, DeskripsiActivity.DISTANCE, this);
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location == null){
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        getDeps().inject(this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        rvListWisata = findViewById(R.id.rv_list_wisata);
        rvListWisata.setLayoutManager(llm);

        adapter = new WisataAdapter(this, list);

        rvListWisata.setAdapter(adapter);
        JsonObject jsonObject = new JsonObject();
        if (getIntent().hasExtra("jenis")){
            jenis = getIntent().getStringExtra("jenis");
        }else {
            jenis = "";
        }


        /*
        jika yang di pilih adalah pantai maka akan memanggil fungsi getWisataByJenis(); dengan wisata
        berjenis pantai, begitu pula ketika gunung dan kuliner
         */
        lat = location.getLatitude();
        lng = location.getLongitude();
        if (jenis.equals("pantai")){
            jsonObject.addProperty("jenis", "pantai");
            jsonObject.addProperty("lat", lat);
            jsonObject.addProperty("lng", lng);
            presenter = new WisataPresenter(this, this, service, jsonObject);
            presenter.getWisataByJenis();
        }else if (jenis.equals("gunung")) {
            jsonObject.addProperty("jenis", "gunung");
            jsonObject.addProperty("lat", lat);
            jsonObject.addProperty("lng", lng);
            presenter = new WisataPresenter(this, this, service, jsonObject);
            presenter.getWisataByJenis();
        }else if (jenis.equals("kuliner")){
            jsonObject.addProperty("jenis", "kuliner");
            jsonObject.addProperty("lat", lat);
            jsonObject.addProperty("lng", lng);
            presenter = new WisataPresenter(this, this, service, jsonObject);
            presenter.getWisataByJenis();
        }else {
            jsonObject.addProperty("lat", lat);
            jsonObject.addProperty("lng", lng);
            presenter = new WisataPresenter(this, this, service, jsonObject);
            presenter.getAllWisata();
            allwisata = true;
        }

    }

    @Override
    public void onShowProgres() {
        progressDialog.show();
    }

    @Override
    public void onHideProgres() {
        progressDialog.dismiss();
    }

    /*
    jika proses request data berhasil, maka fungsi di bawah akan di kerjakan
     */
    @Override
    public void onSuccess(AllWisataResponse allWisataResponse) {
        for (int i = 0; i< allWisataResponse.getData().size(); i++){
            list.add(allWisataResponse.getData().get(i));
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailed(String string) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /*
    fungsi yang digunakan untuk melakukan cek akses lokasi device, sebelum mendapatkan koordinat lokasi di perlukan
    akses lokasi device saat runtime
     */
    private final static int MY_PERMISSIONS_MULTIPLE = 1;
    private boolean checkPermission() {
        if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED &&
                (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED &&
                        (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))))) {

            // Should we show an explanation?
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
            }, MY_PERMISSIONS_MULTIPLE);
        }else {
            permission_granted = true;
        }
        return permission_granted;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 1 && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
           loadView();
        } else {
            Toast.makeText(this, "Lokasi harus diaktifkan", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    //menu ikon maps
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if(allwisata)inflater.inflate(R.menu.menu_maps, menu);
        return true;
    }

    /*
    fungsi ketika menu ikon maps diklik
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.maps_wisata:
                startActivity(new Intent(this, MapsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
