package dev.gilang.wisatagk;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;

/*
adapter digunakan sebagai penampungan data dalam bentuk list
 */
public class WisataAdapter extends RecyclerView.Adapter<WisataAdapter.ItemHolder>{
    private Context context;
    private List<AllWisataResponse.DataWisata> wisataList;

    public WisataAdapter(Context context, List<AllWisataResponse.DataWisata> wisataList) {
        this.context = context;
        this.wisataList = wisataList;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_wisata, viewGroup, false);
        WisataAdapter.ItemHolder item = new WisataAdapter.ItemHolder(view);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder itemHolder, int i) {
        final AllWisataResponse.DataWisata dataWisata = wisataList.get(i);
        itemHolder.wisataName.setText(dataWisata.getNama());
        itemHolder.jarak.setText(String.format(Locale.US, "%s km", Helper.formatNumber(dataWisata.getJarak())));
        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DeskripsiActivity.class);
                    intent.putExtra("wisata", new Gson().toJson(dataWisata));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wisataList.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        TextView wisataName;
        TextView jarak;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            wisataName = itemView.findViewById(R.id.wisata_name);
            jarak = itemView.findViewById(R.id.jarak);
        }
    }
}
