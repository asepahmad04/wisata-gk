package dev.gilang.wisatagk;

import com.google.gson.JsonObject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/*
mengolah data dari server
 */
public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    /*
    RxAndroid
     */
    public Subscription getAllWisata(JsonObject jsonObject, final GetAllWisata callback){
        return networkService.getAllWisata(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AllWisataResponse>>() {
                    @Override
                    public Observable<? extends AllWisataResponse> call(Throwable throwable) {
                        return null;
                    }
                })
                .subscribe(new Subscriber<AllWisataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(AllWisataResponse historyOrderResponse) {
                        callback.onSuccess(historyOrderResponse);
                    }
                });
    }

    public Subscription getWisataByJenis(JsonObject jsonObject, final GetAllWisata callback){
        return networkService.getByJenis(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AllWisataResponse>>() {
                    @Override
                    public Observable<? extends AllWisataResponse> call(Throwable throwable) {
                        return null;
                    }
                })
                .subscribe(new Subscriber<AllWisataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(AllWisataResponse historyOrderResponse) {
                        callback.onSuccess(historyOrderResponse);
                    }
                });
    }

    public interface GetAllWisata{
        void onSuccess(AllWisataResponse response);
        void onError(String networkError);

    }

    public Subscription searchWisata(JsonObject jsonObject, final GetAllWisata callback){
        return networkService.search(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AllWisataResponse>>() {
                    @Override
                    public Observable<? extends AllWisataResponse> call(Throwable throwable) {
                        return null;
                    }
                })
                .subscribe(new Subscriber<AllWisataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(AllWisataResponse historyOrderResponse) {
                        callback.onSuccess(historyOrderResponse);
                    }
                });
    }
}
