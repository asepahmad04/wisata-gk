package dev.gilang.wisatagk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;

import javax.inject.Inject;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback , LocationListener, WisataView{

    @Inject
    public Service service;

    private GoogleMap mMap;
    double lat, lng;
    LocationManager locationManager;
    Location location;

    public static final int  REQUEST_TIME = 5000;
    public static final int DISTANCE = 100;

    WisataPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getSupportActionBar().setTitle(getString(R.string.peta_lokasi_wisata_gk));

        getDeps().inject(this);

        checkPermission();
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, REQUEST_TIME, DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REQUEST_TIME, DISTANCE, this);
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location == null){
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("lat", lat);
        jsonObject.addProperty("lng", lng);
        presenter = new WisataPresenter(this, this, service, jsonObject);
        presenter.getAllWisata();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 20);
        mMap.moveCamera(cameraUpdate);
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_MULTIPLE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    Log.d("permission denied", "permission denied Disable");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private final static int MY_PERMISSIONS_MULTIPLE = 1;
    private void checkPermission() {
        if ((ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))) {

            // Should we show an explanation?
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
            }, MY_PERMISSIONS_MULTIPLE);
        }
    }

    @Override
    public void onShowProgres() {

    }

    @Override
    public void onHideProgres() {

    }

    @Override
    public void onSuccess(AllWisataResponse allWisataResponse) {
        for (int i = 0; i< allWisataResponse.getData().size(); i++){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.valueOf(allWisataResponse.getData().get(i).getLat()),
                            Double.valueOf(allWisataResponse.getData().get(i).getLng())))
                    .title(allWisataResponse.getData().get(i).getNama()));

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Intent intent = new Intent(MapsActivity.this, RouteActivity.class);
                    intent.putExtra("flat", ""+lat);
                    intent.putExtra("flng", ""+lng);
                    intent.putExtra("tlat", ""+marker.getPosition().latitude);
                    intent.putExtra("tlng", ""+marker.getPosition().longitude);
                    intent.putExtra("wisata_name", marker.getTitle());
                    startActivity(intent);
                }
            });
        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 20);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onFailed(String string) {

    }
    
}
