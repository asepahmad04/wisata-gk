package dev.gilang.wisatagk;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/*
activity yang di gunakan untuk mencari lokasi wisata berdasarkan keyword yang di masukkan pengguna
 */
public class SearchActivity extends BaseActivity implements WisataView{
    @Inject public Service service;
    WisataPresenter presenter;
    ProgressDialog progressDialog;
    WisataAdapter adapter;
    List<AllWisataResponse.DataWisata> list = new ArrayList<>();
    RecyclerView rvListWisata;
    EditText editText;
    LocationTrack locationTrack;
    double lat = 0D, lng = 0D;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        locationTrack = new LocationTrack(this);
        lat = locationTrack.getLatitude();
        lng = locationTrack.getLongitude();
        progressDialog = Helper.initProgressDialog(this, getString(R.string.please_wait));
        getDeps().inject(this);

        getSupportActionBar().setTitle(getString(R.string.search_activity));

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        rvListWisata = findViewById(R.id.rv_list_wisata);
        editText = findViewById(R.id.name);

        rvListWisata.setLayoutManager(llm);

        adapter = new WisataAdapter(this, list);
        rvListWisata.setAdapter(adapter);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("nama", s.toString());
                jsonObject.addProperty("lat", String.valueOf(lat));
                jsonObject.addProperty("lng", String.valueOf(lng));
                presenter = new WisataPresenter(SearchActivity.this, SearchActivity.this, service, jsonObject);
                presenter.searchWisata();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onShowProgres() {
        progressDialog.show();
    }

    @Override
    public void onHideProgres() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(AllWisataResponse allWisataResponse) {
        list.clear();
        for (int i = 0; i< allWisataResponse.getData().size(); i++){
            list.add(allWisataResponse.getData().get(i));
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailed(String string) {

    }
}
