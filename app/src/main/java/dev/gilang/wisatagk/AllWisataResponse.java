package dev.gilang.wisatagk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
//merubah data dari server JSON ke bentuk kelas
public class AllWisataResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DataWisata> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataWisata> getData() {
        return data;
    }

    public void setData(List<DataWisata> data) {
        this.data = data;
    }

    public class DataWisata{
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;
        @SerializedName("gambar")
        @Expose
        private String gambar;
        @SerializedName("jarak")
        @Expose
        private String jarak;

        public String getGambar() {
            return gambar;
        }

        public void setGambar(String gambar) {
            this.gambar = gambar;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getJarak() {
            return jarak;
        }

        public void setJarak(String jarak) {
            this.jarak = jarak;
        }
    }
}
