package dev.gilang.wisatagk;

import android.content.Context;

import com.google.gson.JsonObject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class WisataPresenter {
    Context context;
    WisataView view;
    private final Service service;
    private CompositeSubscription subscriptions;
    JsonObject jsonObject;

    /*
    presenter akan menjadi penghubung antara activity (view) dengan end point yang digunakan
    fungsi2 end poin yang di gunakan berapa pada service
     */
    public WisataPresenter(Context context, WisataView view, Service service) {
        this.context = context;
        this.view = view;
        this.service = service;
        this.subscriptions = new CompositeSubscription();
    }

    public WisataPresenter(Context context, WisataView view, Service service, JsonObject jsonObject) {
        this.context = context;
        this.view = view;
        this.service = service;
        this.subscriptions = new CompositeSubscription();
        this.jsonObject = jsonObject;
    }

    public void getAllWisata(){
        view.onShowProgres();
        Subscription subscription = service.getAllWisata(jsonObject, new Service.GetAllWisata() {
                    @Override
                    public void onSuccess(AllWisataResponse response) {
                        view.onHideProgres();
                        view.onSuccess(response);
                    }

                    @Override
                    public void onError(String networkError) {
                        view.onHideProgres();
                        view.onFailed(networkError);
                    }
                });

        subscriptions.add(subscription);
    }
    //mengrim parameter ke rx android
    public void getWisataByJenis(){
        view.onShowProgres();
        Subscription subscription = service.getWisataByJenis(jsonObject, new Service.GetAllWisata() {
                    @Override
                    public void onSuccess(AllWisataResponse response) {
                        view.onHideProgres();
                        view.onSuccess(response);
                    }

                    @Override
                    public void onError(String networkError) {
                        view.onHideProgres();
                        view.onFailed(networkError);
                    }
                });

        subscriptions.add(subscription);
    }

    public void searchWisata(){
        view.onShowProgres();
        Subscription subscription = service.searchWisata(jsonObject, new Service.GetAllWisata() {
                    @Override
                    public void onSuccess(AllWisataResponse response) {
                        view.onHideProgres();
                        view.onSuccess(response);
                    }

                    @Override
                    public void onError(String networkError) {
                        view.onHideProgres();
                        view.onFailed(networkError);
                    }
                });

        subscriptions.add(subscription);
    }
}
