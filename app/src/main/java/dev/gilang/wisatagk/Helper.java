package dev.gilang.wisatagk;

import android.app.ProgressDialog;
import android.content.Context;

import java.text.DecimalFormat;

public class Helper {
    public static ProgressDialog initProgressDialog(Context context, String message){
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        return dialog;
    }

    public static String formatNumber(String s){
        DecimalFormat df2 = new DecimalFormat("#.#");
        return df2.format(Double.parseDouble(s));
    }
}
