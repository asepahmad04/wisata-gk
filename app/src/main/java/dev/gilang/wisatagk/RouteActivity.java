package dev.gilang.wisatagk;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mapbox.directions.DirectionsCriteria;
import com.mapbox.directions.MapboxDirections;
import com.mapbox.directions.service.models.DirectionsResponse;
import com.mapbox.directions.service.models.DirectionsRoute;
import com.mapbox.directions.service.models.Waypoint;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/*
class yang di gunakan untuk membuat rute menuju lokasi wisata
 */
public class RouteActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    double fLat, fLng, tLat, tLng;
    String wisataName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        getSupportActionBar().setTitle(getString(R.string.routing));
        fLat = Double.valueOf(getIntent().getStringExtra("flat"));
        fLng = Double.valueOf(getIntent().getStringExtra("flng"));
        tLat = Double.valueOf(getIntent().getStringExtra("tlat"));
        tLng = Double.valueOf(getIntent().getStringExtra("tlng"));
        wisataName = getIntent().getStringExtra("wisata_name");

        Log.e("ROUTE ", ""+fLat+fLng+tLat+tLng);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng point = new LatLng(tLat, tLng);
        mMap.addMarker(new MarkerOptions().position(point).title(wisataName));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(fLat, fLng)));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(fLat, fLng), 15);
        mMap.animateCamera(cameraUpdate);

        Waypoint origin = new Waypoint(fLng, fLat);

        Waypoint destination = new Waypoint(tLng, tLat);

        // Build the client object
        MapboxDirections client = new MapboxDirections.Builder()
                .setAccessToken(getString(R.string.token))
                .setOrigin(origin)
                .setDestination(destination)
                .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                .build();

        /*
        response dari server mapbox akan di terjemahkan menjadi beberapa point yang kemudian
        di tampilkan pada peta
         */
        client.enqueue(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Response<DirectionsResponse> response, Retrofit retrofit) {
                // Convert List into LatLng[]
                try {
                    DirectionsRoute currentRoute = response.body().getRoutes().get(0);
                    List<Waypoint> waypoints = currentRoute.getGeometry().getWaypoints();
                    LatLng[] points = new LatLng[waypoints.size()];
                    for (int i = 0; i < waypoints.size(); i++) {
                        points[i] = new LatLng(
                                waypoints.get(i).getLatitude(),
                                waypoints.get(i).getLongitude());
                    }
                    // Instantiates a new Polyline object and adds points to define a rectangle
                    PolylineOptions rectOptions = new PolylineOptions()
                            .color(getResources().getColor(R.color.colorPrimary)); // Closes the polyline.
                    for (int i = 0; i< points.length; i++){
                        rectOptions.add(points[i]);
                    }
                    // Get back the mutable Polyline
                    mMap.addPolyline(rectOptions);
                }catch (Exception e){
                    e.printStackTrace();
                    messagePopUp();
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    /*
    jika rute dengan menggunakan mapbox tidak di temukan, maka akan ditampilkan dialog yang akan
    mengarahkan rute dengan menggunakaan google map
     */
    Dialog dialog;
    private void messagePopUp() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_confirm, null, false);

        final TextView tvOk = view.findViewById(R.id.ok);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+fLat+","+fLng+"&daddr="+tLat+","+tLng+""));
                startActivity(intent);
            }
        });

        builder.setView(view);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

}
