package dev.gilang.wisatagk;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.Locale;

/*
kelas yang di gunakan untuk menampilkan detai informasi wisata
 */
public class DeskripsiActivity extends AppCompatActivity implements LocationListener{
    private static final String TAG = DeskripsiActivity.class.getSimpleName();
    TextView nama, alamat, deskripsi;
    ImageView imgroute, imageView;
    LocationManager locationManager;
    Location location;
    public static final int  REQUEST_TIME = 5000;
    public static final int DISTANCE = 100;


    double lat, lng;
    private AllWisataResponse.DataWisata dataWisata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deskripsi_avtivity);
        nama = findViewById(R.id.nama);
        alamat  = findViewById(R.id.alamat);
        deskripsi = findViewById(R.id.deskripsi);
        imgroute = findViewById(R.id.imgroute);
        imageView = findViewById(R.id.imageView);

        dataWisata = new Gson().fromJson(getIntent().getStringExtra("wisata"), AllWisataResponse.DataWisata.class);
        nama.setText(dataWisata.getNama());
        alamat.setText(dataWisata.getAlamat());
        deskripsi.setText(dataWisata.getDeskripsi());

        Glide.with(this).load(dataWisata.getGambar()).into(imageView);

        checkPermission();
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, REQUEST_TIME, DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REQUEST_TIME, DISTANCE, this);
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location == null){
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        /*
        action yang akan menuju kelas RouteActivity ketika gambar lokasi di klik
         */
        imgroute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeskripsiActivity.this, RouteActivity.class);
                intent.putExtra("flat", ""+lat);
                intent.putExtra("flng", ""+lng);
                intent.putExtra("tlat", ""+dataWisata.getLat());
                intent.putExtra("tlng", ""+dataWisata.getLng());
                intent.putExtra("wisata_name", dataWisata.getNama());
                startActivity(intent);
            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();

        Log.e("LATLNG", ""+lat+lng);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /*
    fungsi yang digunakan untuk melakukan cek akses lokasi device, sebelum mendapatkan koordinat lokasi di perlukan
    akses lokasi device saat runtime
     */
    private final static int MY_PERMISSIONS_MULTIPLE = 1;
    private void checkPermission() {
        if ((ContextCompat.checkSelfPermission(DeskripsiActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED &&
                (ContextCompat.checkSelfPermission(DeskripsiActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED &&
                        (ContextCompat.checkSelfPermission(DeskripsiActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                (ContextCompat.checkSelfPermission(DeskripsiActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))))) {

            // Should we show an explanation?
            ActivityCompat.requestPermissions(DeskripsiActivity.this, new String[]{
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
            }, MY_PERMISSIONS_MULTIPLE);
        }
    }

}
