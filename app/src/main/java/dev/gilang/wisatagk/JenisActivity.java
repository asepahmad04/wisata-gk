package dev.gilang.wisatagk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
/*
activty untuk menampilkan jenis-jenis wisata yang ada pada aplikasi
 */
public class JenisActivity extends AppCompatActivity {

    TextView pantai, gunung, kuliner ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenis);

        pantai = findViewById(R.id.pantai);
        gunung = findViewById(R.id.gunung);
        kuliner = findViewById(R.id.kuliner);



        /*
        jika pantai di klik maka akan mengirimkan inten dengan data pantai
         */
        pantai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JenisActivity.this, MainActivity.class);
                intent.putExtra("jenis", "pantai");
                startActivity(intent);
            }
        });

        /*
        jika gunung di klik maka akan mengirimkan inten dengan data gunung
         */
        gunung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JenisActivity.this, MainActivity.class);
                intent.putExtra("jenis", "gunung");
                startActivity(intent);
            }
        });

        /*
        jika kuliner di klik maka akan mengirimkan inten dengan data kuliner
         */
        kuliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JenisActivity.this, MainActivity.class);
                intent.putExtra("jenis", "kuliner");
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /*
    fungsi ketika menu di sebelah kanan(titik tiga di klik)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list_wisata:
                startActivity(new Intent(this, MainActivity.class));
                return true;
            case R.id.search_wisata:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            case R.id.bantuan:
                startActivity(new Intent(this, HelpActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
